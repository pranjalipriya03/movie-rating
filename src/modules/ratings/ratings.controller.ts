import { Controller, Get, Post, Body, Patch, Param, Delete, UploadedFile, UseInterceptors } from '@nestjs/common';
import { RatingsService } from './ratings.service';
import { multerOptions } from '../../utils/multer-upload';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('')
export class RatingsController {
  constructor(private readonly ratingsService: RatingsService) { }

  @Post('upload')
  @UseInterceptors(FileInterceptor('file', multerOptions))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File) {
    return this.ratingsService.upload(file);
  }

}
