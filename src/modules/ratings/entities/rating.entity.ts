import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
import { Movie } from '../../movies/entities/movie.entity';


@Entity()
export class Rating {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('numeric', { precision: 10, scale: 2 })
    averageRating: number;

    @Column()
    numVotes: number;

    @OneToOne(() => Movie)
    @JoinColumn({ name: 'tconst' })
    tconst: Movie;
}