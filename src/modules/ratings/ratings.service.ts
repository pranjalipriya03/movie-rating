import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Rating } from './entities/rating.entity';
import { Repository } from 'typeorm';
import * as XLSX from 'xlsx';

@Injectable()
export class RatingsService {
  constructor(
    @InjectRepository(Rating)
    private readonly ratingRepository: Repository<Rating>,
  ) { }
  async upload(file) {

    const workbook = XLSX.readFile(file.path);
    const sheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[sheetName];
    const jsonData = XLSX.utils.sheet_to_json(worksheet);
    await this.create(jsonData)
  }

  async create(data: any) {
    return await this.ratingRepository.insert(data);
  }

  async getTopRatedMovies() {
    const movies = await this.ratingRepository
      .createQueryBuilder('rating')
      .leftJoinAndSelect('rating.tconst', 'movie')
      .select([
        'movie.tconst AS tconst',
        'movie.primaryTitle AS primaryTitle',
        'movie.genres AS genre',
        'rating.averageRating AS averageRating',
      ])
      .where('rating.averageRating > :rating', { rating: 6.0 })
      .orderBy('rating.averageRating', 'DESC')
      .getRawMany();

    return movies;
  }

  async getGenreMoviesWithSubtotal() {

    const query = await this.ratingRepository.createQueryBuilder('rating')
      .leftJoin('rating.tconst', 'movie')
      .select([
        'movie.genres as genres',
        'movie.primaryTitle as primaryTitle',
        'rating.numVotes as numVotes',
      ])
      .groupBy('movie.genres, movie.primaryTitle')
      .addGroupBy('rating.numVotes')
      .orderBy('movie.genres, movie.primaryTitle')
      .getRawMany();
    const result = await query;

    const genreTotals = await this.ratingRepository.createQueryBuilder('rating')
      .leftJoin('rating.tconst', 'movie')
      .select([
        'movie.genres',
        'SUM(rating.numVotes) as TOTAL',
      ])
      .groupBy('movie.genres')
      .orderBy('movie.genres')
      .getRawMany();



    return { genreTotals, result }
  }

}
