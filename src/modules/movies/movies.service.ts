import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Movie } from './entities/movie.entity';
import { Repository } from 'typeorm';
import * as XLSX from 'xlsx';
import { RatingsService } from '../ratings/ratings.service';

@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(Movie)
    private readonly movieRepository: Repository<Movie>,
    private readonly ratingService: RatingsService
  ) { }

  async upload(file) {

    const workbook = XLSX.readFile(file.path);
    const sheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[sheetName];
    const jsonData = XLSX.utils.sheet_to_json(worksheet);
    await this.create(jsonData)
  }

  async create(data: any) {
    return await this.movieRepository.save(data);
  }

  async getLongestDurationMovies(limit: number) {
    return this.movieRepository.find({
      select: ['tconst', 'primaryTitle', 'runtimeMinutes', 'genres'],
      order: { runtimeMinutes: 'DESC' },
      take: limit,
    });
  }

  async createMovie(movieData: any) {
    await this.movieRepository.save(movieData);
  }


  async getTopRatedMovies() {
    const movies = await this.ratingService.getTopRatedMovies()
    return movies;
  }

  async updateRuntimeMinutes(): Promise<void> {
    await this.movieRepository.createQueryBuilder()
      .update()
      .set({
        runtimeMinutes: () => `
        CASE
          WHEN genres = 'Documentary' THEN runtimeMinutes + 15
          WHEN genres = 'Animation' THEN runtimeMinutes + 30
          ELSE runtimeMinutes + 45
        END
      `,
      })
      .execute();
  }











  async getGenreMoviesWithSubtotals() {
    return this.ratingService.getGenreMoviesWithSubtotal();
  }




}