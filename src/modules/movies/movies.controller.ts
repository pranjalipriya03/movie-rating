import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile } from '@nestjs/common';
import { MoviesService } from './movies.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { multerOptions } from '../../utils/multer-upload';


@Controller('/api/v1')
export class MoviesController {
  constructor(private readonly moviesService: MoviesService) { }

  @Post('upload')
  @UseInterceptors(FileInterceptor('file', multerOptions))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File) {
    return this.moviesService.upload(file);
  }

  @Get('longest-duration-movies')
  async getLongestDurationMovies() {
    return this.moviesService.getLongestDurationMovies(10);
  }

  @Post('new-movie')
  async createNewMovie(@Body() movieData: any) {
    await this.moviesService.createMovie(movieData);
    return 'success';
  }

  @Get('top-rated-movies')
  async getTopRatedMovies() {
    return this.moviesService.getTopRatedMovies();
  }

  @Post('update-runtime-minutes')
  async updateRuntimeMinutes() {
    await this.moviesService.updateRuntimeMinutes();
  }






  @Get('genre-movies-with-subtotals')
  async getGenreMoviesWithSubtotals() {
    return this.moviesService.getGenreMoviesWithSubtotals();
  }
}
