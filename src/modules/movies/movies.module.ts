import { Module } from '@nestjs/common';
import { MoviesService } from './movies.service';
import { MoviesController } from './movies.controller';
import { Movie } from './entities/movie.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RatingsModule } from '../ratings/ratings.module';
import { RatingsService } from '../ratings/ratings.service';
import { Rating } from '../ratings/entities/rating.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Movie, Rating]), RatingsModule],
  controllers: [MoviesController],
  providers: [MoviesService, RatingsService]
})
export class MoviesModule { }
