import { Entity, Column, PrimaryGeneratedColumn, PrimaryColumn } from 'typeorm';


@Entity()
export class Movie {

    @PrimaryColumn()
    tconst: string;

    @Column()
    titleType: string;

    @Column()
    primaryTitle: string;

    @Column()
    runtimeMinutes: number;

    @Column()
    genres: string;
}