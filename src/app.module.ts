import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MoviesModule } from './modules/movies/movies.module';
import { RatingsModule } from './modules/ratings/ratings.module';
import { ConfigModule } from '@nestjs/config';
import { ConnectionModule } from './modules/connection/connection.module';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), ConnectionModule, MoviesModule, RatingsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
